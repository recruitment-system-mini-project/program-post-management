package api

import (
	"database/sql"
	"log"
	"time"

	"enigmacamp.com/recruitment_system/config"
	"enigmacamp.com/recruitment_system/delivery"
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/manager"
)

type Server interface {
	Run()
}

type server struct {
	config  *config.Config
	infra   manager.Infra
	usecase manager.UseCaseManager
}

func (s *server) Run() {
	if !(s.config.RunMigration == "Y" || s.config.RunMigration == "y") {
		db, _ := s.infra.SqlDb().DB()
		defer func(db *sql.DB) {
			err := db.Close()
			if err != nil {
				log.Panicln(err)
			}
		}(db)
		s.InitRouter()
		err := s.config.RouterEngine.Run(s.config.ApiBaseUrl)
		if err != nil {
			log.Panicln(err)
		}
	} else {
		db := s.infra.SqlDb()
		err := db.AutoMigrate(
			&entity.ProgramPost{},
			&entity.ProgramActivity{},
			&entity.ProgramType{},
			&entity.ProgramLocation{},
			&entity.RecruiterProfile{},
			&entity.Applicant{},
			&entity.ProgramApplicant{},

			// &entity.ProgramApplicantTest{},
			// &entity.ApplicantTest{},
			// &entity.ProgramPostTest{},
		)
		if err != nil {
			log.Panicln(err)
		}

		db.Unscoped().Where("id like ?", "%%").Delete(entity.ProgramApplicant{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.Applicant{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.ProgramActivity{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.ProgramLocation{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.ProgramPost{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.ProgramType{})
		db.Unscoped().Where("id like ?", "%%").Delete(entity.RecruiterProfile{})

		db.Model(&entity.Applicant{}).Save([]entity.Applicant{
			{
				ID:    "A0001",
				Name:  "Gde Indra",
				Email: "gdeindra@smm.com",
			},
			{
				ID:    "A0002",
				Name:  "Giovvani",
				Email: "gio@smm.com",
			},
		})

		db.Model(&entity.RecruiterProfile{}).Save([]entity.RecruiterProfile{
			{
				ID:           "R0001",
				Fullname:     "Nurmalita",
				Nip:          "12345678",
				Email:        "nurmalita@enigmacamp.com",
				JobPosition:  "HR",
				BussinesUnit: "Enigma Camp",
			},
		})

		db.Model(&entity.ProgramType{}).Save([]entity.ProgramType{
			{
				ID:          "PT001",
				ProgramName: "Talent Development",
			},
			{
				ID:          "PT002",
				ProgramName: "Certification",
			},
			{
				ID:          "PT003",
				ProgramName: "Training",
			},
			{
				ID:          "PT004",
				ProgramName: "Webinar",
			},
		})

		db.Model(&entity.ProgramPost{}).Save([]entity.ProgramPost{
			{
				ID: "PR0001",
				// ProgramType: entity.ProgramType{
				// 	ID: "PT001",
				// },
				ProgramLocation: entity.ProgramLocation{
					Address:  "Online",
					Platform: "Zoom",
				},
				ProgramActivity: entity.ProgramActivity{
					OpenDate:      time.Date(2022, 3, 1, 0, 0, 0, 0, time.UTC),
					CloseDate:     time.Date(2022, 3, 20, 0, 0, 0, 0, time.UTC),
					ProcessStatus: "Ongoing",
				},
				Headline:    "ENDP - Coal Processing & Quality Control",
				Description: "1. Membuat rencana penambangan bulanan, mingguan, harian \n 2. Monitoring dan pengawasan terhadap implementasi rencana kerja penambangan \n 3. Menganalisa dan mengevaluasi rencana penambangan serta realisasinya \n 4. Melakukan pengembangan terhadap rencana kerja penambanga \n 5. Membuat laporan penambangan bulanan, mingguan, harian",
				Requirement: "1. Memiliki pengalaman di bidang Coal Processing & Quality Control",
				IsActive:    true,
			},
			{
				ID: "PR0002",
				// ProgramType: entity.ProgramType{
				// 	ID: "PT002",
				// },
				ProgramLocation: entity.ProgramLocation{
					Address:  "Online",
					Platform: "Zoom",
				},
				ProgramActivity: entity.ProgramActivity{
					OpenDate:      time.Date(2022, 3, 1, 0, 0, 0, 0, time.UTC),
					CloseDate:     time.Date(2022, 3, 20, 0, 0, 0, 0, time.UTC),
					ProcessStatus: "Ongoing",
				},
				Headline:    "Fullstack Developer Certification",
				Description: "1. Menyelesaikan semua materi training yang akan diberikan \n 2. Menyelesaikan task harian \n 3. Menyelesaikan ujian nilai akhir sertifikasi",
				Requirement: "1. Menguasai golang dan react.js",
				IsActive:    true,
			},
		})

	}
}
func (s *server) InitRouter() {
	publicRoute := s.config.RouterEngine.Group("/api")
	delivery.NewProgramPostApi(publicRoute, s.usecase.ProgramPostUseCase())
	delivery.NewRecruiterApi(publicRoute, s.usecase.RecruiterUseCase())
	delivery.NewProgramApplicantApi(publicRoute, s.usecase.ProgramApplicantUseCase())
}

func NewApiServer() Server {
	appConfig := config.NewConfig()
	infra := manager.NewInfra(appConfig)
	repo := manager.NewRepoManager(infra)
	usecase := manager.NewUseCaseManager(repo)

	return &server{
		config:  appConfig,
		infra:   infra,
		usecase: usecase,
	}
}
