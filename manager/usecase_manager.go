package manager

import "enigmacamp.com/recruitment_system/usecase"

type UseCaseManager interface {
	ProgramPostUseCase() usecase.ProgramPostUseCase
	RecruiterUseCase() usecase.RecruiterUseCase
	ProgramApplicantUseCase() usecase.ProgramApplicantUseCase
}

type usecaseManager struct {
	repo RepoManager
}

func (uc *usecaseManager) ProgramPostUseCase() usecase.ProgramPostUseCase {
	return usecase.NewProgramPostUseCase(uc.repo.ProgramPostRepo())
}

func (uc *usecaseManager) RecruiterUseCase() usecase.RecruiterUseCase {
	return usecase.NewRecruiterUseCase(uc.repo.RecruiterRepo())
}

func (uc *usecaseManager) ProgramApplicantUseCase() usecase.ProgramApplicantUseCase {
	return usecase.NewProgramApplicantUseCase(uc.repo.ProgramApplicantRepo())
}

func NewUseCaseManager(repoManager RepoManager) UseCaseManager {
	return &usecaseManager{
		repo: repoManager,
	}
}
