package manager

import "enigmacamp.com/recruitment_system/repository"

type RepoManager interface {
	ProgramPostRepo() repository.ProgramPostRepository
	RecruiterRepo() repository.RecruiterRepository
	ProgramApplicantRepo() repository.ProgramApplicantRepository
}

type repoManager struct {
	infra Infra
}

func (r *repoManager) ProgramPostRepo() repository.ProgramPostRepository {
	return repository.NewProgramPostRepository(r.infra.SqlDb())
}

func (r *repoManager) RecruiterRepo() repository.RecruiterRepository {
	return repository.NewRecuiterRepository(r.infra.SqlDb())
}

func (r *repoManager) ProgramApplicantRepo() repository.ProgramApplicantRepository {
	return repository.NewProgramApplicantRepository(r.infra.SqlDb())
}

func NewRepoManager(infra Infra) RepoManager {
	return &repoManager{
		infra: infra,
	}
}
