package entity

import (
	"gorm.io/gorm"
)

type BussinesUnit struct {
	BuID   string
	BuName string
	gorm.Model
}

func (b *BussinesUnit) TableName() string {
	return "bussiness_unit"
}
