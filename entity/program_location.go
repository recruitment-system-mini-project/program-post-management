package entity

import (
	guuid "github.com/google/uuid"
	"gorm.io/gorm"
)

type ProgramLocation struct {
	ID            string `gorm:"column:id;size:36;primaryKey"`
	Address       string
	Platform      string
	ProgramPostId string
	gorm.Model
}

func (p *ProgramLocation) TableName() string {
	return "program_location"
}

func (p *ProgramLocation) BeforeCreate(tx *gorm.DB) error {
	p.ID = guuid.New().String()
	return nil
}
