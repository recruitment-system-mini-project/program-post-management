package entity

import (
	"time"

	guuid "github.com/google/uuid"
	"gorm.io/gorm"
)

type ProgramActivity struct {
	ID            string `gorm:"column:id;size:36;primaryKey"`
	OpenDate      time.Time
	CloseDate     time.Time
	ProcessStatus string
	ProgramPostId string
	gorm.Model
}

func (p *ProgramActivity) TableName() string {
	return "program_activity"
}

func (p *ProgramActivity) BeforeCreate(tx *gorm.DB) error {
	p.ID = guuid.New().String()
	return nil
}
