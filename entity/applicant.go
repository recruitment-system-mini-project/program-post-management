package entity

import "gorm.io/gorm"

type Applicant struct {
	ID          string `gorm:"column:id;size:5;primaryKey"`
	Name        string
	Email       string
	ProgramPosts []ProgramPost `gorm:"many2many:program_applicants;"`
	gorm.Model
}

func (a *Applicant) TableName() string {
	return "applicant"
}
