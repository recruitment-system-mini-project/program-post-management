package entity

import (
	"gorm.io/gorm"
)

type RecruiterProfile struct {
	ID           string `gorm:"column:id;size:5;primaryKey"`
	Fullname     string
	Nip          string
	Email        string
	JobPosition  string
	PhotoProfile string
	BussinesUnit string
	// BussinessUnitID string `gorm:"references:BuID"`
	// BussinessUnit   BussinesUnit
	gorm.Model
}

func (r *RecruiterProfile) TableName() string {
	return "recruiter_profile"
}

// func (r *RecruiterProfile) BeforeCreate(tx *gorm.DB) error {
// 	r.ID = guuid.New().String()
// 	return nil
// }
