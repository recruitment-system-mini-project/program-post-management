package entity

import (
	"gorm.io/gorm"
)

type ProgramType struct {
	ID          string      `gorm:"column:id;size:5;primaryKey"`
	ProgramName string      `gorm:"size:20"`
	gorm.Model
}

func (p *ProgramType) TableName() string {
	return "program_type"
}

// func (p *ProgramType) BeforeCreate(tx *gorm.DB) error {
// 	p.ID = guuid.New().String()
// 	return nil
// }
