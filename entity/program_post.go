package entity

import (
	"gorm.io/gorm"
)

type ProgramPost struct {
	ID string `gorm:"column:id;size:36;primaryKey"`
	// ProgramTypeID   string
	// ProgramType     ProgramType
	ProgramLocation ProgramLocation
	ProgramActivity ProgramActivity
	Headline        string
	Description     string
	Requirement     string
	IsActive        bool
	Applicants      []Applicant `gorm:"many2many:program_applicants;"`
	gorm.Model
}

func (p *ProgramPost) TableName() string {
	return "program_post"
}

// func (p *ProgramPost) BeforeCreate(tx *gorm.DB) error {
// 	p.ID = guuid.New().String()
// 	return nil
// }
