package entity

import (
	"gorm.io/gorm"
)

type ProgramPostTest struct {
	ID             string `gorm:"column:id;size:36;primaryKey"`
	Headline       string
	Description    string
	Requirement    string
	IsActive       bool
	ApplicantTests []ApplicantTest `gorm:"many2many:program_applicant_tests;"`
	gorm.Model
}

type ApplicantTest struct {
	ID    string `gorm:"column:id;size:5;primaryKey"`
	Name  string
	Email string
}

type ProgramApplicantTest struct {
	ProgramPostTestID string `gorm:"primaryKey"`
	ApplicantTestID   string `gorm:"primaryKey"`
	Test              bool
	Status            string
}
