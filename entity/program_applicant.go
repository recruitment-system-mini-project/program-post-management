package entity

import (
	guuid "github.com/google/uuid"
	"gorm.io/gorm"
)

type ProgramApplicant struct {
	ID            string `gorm:"column:id;size:36;primaryKey"`
	ProgramPostId string
	ApplicantId   string
	ProcessStatus string
	Accepted      bool
	gorm.Model
}

func (p *ProgramApplicant) TableName() string {
	return "program_applicant"
}

func (p *ProgramApplicant) BeforeCreate(tx *gorm.DB) error {
	p.ID = guuid.New().String()
	return nil
}
