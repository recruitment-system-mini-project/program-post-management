package utils

import (
	"time"

	"enigmacamp.com/recruitment_system/dto"
	"enigmacamp.com/recruitment_system/entity"
)

type MapProgramRequest struct {
	ProgramRequest dto.ProgramPostRequest
}

func (m *MapProgramRequest) MappingProgramPostRequest() entity.ProgramPost {
	openDate, _ := time.Parse("2006-01-02", m.ProgramRequest.ProgramActivity.OpenDate)
	closeDate, _ := time.Parse("2006-01-02", m.ProgramRequest.ProgramActivity.CloseDate)

	return entity.ProgramPost{
		ID: m.ProgramRequest.ID,
		// ProgramType: entity.ProgramType{
		// 	ID: m.ProgramRequest.ProgramTypeID,
		// },
		ProgramLocation: entity.ProgramLocation{
			ID:       m.ProgramRequest.ProgramLocation.ID,
			Address:  m.ProgramRequest.ProgramLocation.Address,
			Platform: m.ProgramRequest.ProgramLocation.Platform,
		},
		ProgramActivity: entity.ProgramActivity{
			ID:            m.ProgramRequest.ProgramActivity.ID,
			OpenDate:      openDate,
			CloseDate:     closeDate,
			ProcessStatus: m.ProgramRequest.ProgramActivity.ProcessStatus,
		},
		Headline:    m.ProgramRequest.Headline,
		Description: m.ProgramRequest.Description,
		Requirement: m.ProgramRequest.Requirement,
		IsActive:    m.ProgramRequest.IsActive,
	}
}

func NewProgramResponse(program dto.ProgramPostRequest) *MapProgramRequest {
	return &MapProgramRequest{ProgramRequest: program}
}
