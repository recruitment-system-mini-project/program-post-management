package delivery

import (
	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/delivery/appresponse"
	"enigmacamp.com/recruitment_system/dto"
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/usecase"
	"github.com/gin-gonic/gin"
)

type ProgramApplicantApi struct {
	usecase     usecase.ProgramApplicantUseCase
	publicRoute *gin.RouterGroup
}

func NewProgramApplicantApi(publicRoute *gin.RouterGroup, useCase usecase.ProgramApplicantUseCase) *ProgramApplicantApi {
	api := ProgramApplicantApi{
		usecase:     useCase,
		publicRoute: publicRoute,
	}

	api.initRouter()
	return &api
}

func (p *ProgramApplicantApi) initRouter() {
	programRoute := p.publicRoute.Group("/program_applicant")
	programRoute.POST("", p.applyProgram)
	programRoute.GET("applicant", p.getAllProgram)
	programRoute.GET("program", p.getAllApplicant)
}

func (p *ProgramApplicantApi) applyProgram(c *gin.Context) {
	var newApplyRequest dto.ApplyProgramRequest
	response := appresponse.NewJsonResponse(c)

	if err := c.ShouldBindJSON(&newApplyRequest); err != nil {
		response.SendError(appresponse.NewBadRequestError(err, "Failed to input a new program post"))
		return
	}

	isExist, err := p.usecase.AppliedProgramIsExist(newApplyRequest.ProgramId, newApplyRequest.ApplicantId)
	if err != nil {
		if err == apperror.RecordIsExistErr {
			response.SendError(appresponse.NewBadRequestError(err, "Program has been applied by applicant"))
			return
		}
		response.SendError(appresponse.NewInternalServerError(err, "Failed to input a new program post"))
		return
	}

	applyProgram := entity.ProgramApplicant{
		ApplicantId:   newApplyRequest.ApplicantId,
		ProgramPostId: newApplyRequest.ProgramId,
		ProcessStatus: "Applied",
	}

	if !isExist {
		_, err = p.usecase.ApplyProgram(applyProgram)
		if err != nil {
			response.SendError(appresponse.NewInternalServerError(err, "Failed to input a new program post"))
			return
		}
	}

	response.SendData(appresponse.NewResponseMessage("SUCCESS", "Apply a program", nil))
}

func (p *ProgramApplicantApi) getAllProgram(c *gin.Context) {
	id := c.Query("id")

	response := appresponse.NewJsonResponse(c)

	data, err := p.usecase.GetAllProgramByApplicant(id)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed"))
		return
	}

	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Applied porgrams by applicants", data))
}

func (p *ProgramApplicantApi) getAllApplicant(c *gin.Context) {
	id := c.Query("id")

	response := appresponse.NewJsonResponse(c)

	data, err := p.usecase.GetAllApplicantByProgram(id)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed"))
		return
	}

	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Applied porgrams by applicants", data))
}
