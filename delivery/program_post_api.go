package delivery

import (
	"strconv"

	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/delivery/appresponse"
	"enigmacamp.com/recruitment_system/delivery/utils"
	"enigmacamp.com/recruitment_system/dto"
	"enigmacamp.com/recruitment_system/usecase"
	"github.com/gin-gonic/gin"
)

type ProgramPostApi struct {
	usecase     usecase.ProgramPostUseCase
	publicRoute *gin.RouterGroup
}

func NewProgramPostApi(publicRoute *gin.RouterGroup, useCase usecase.ProgramPostUseCase) *ProgramPostApi {
	api := ProgramPostApi{
		usecase:     useCase,
		publicRoute: publicRoute,
	}

	api.initRouter()
	return &api
}

func (p *ProgramPostApi) initRouter() {
	programRoute := p.publicRoute.Group("/program")
	programRoute.GET("", p.getProgram)
	programRoute.POST("", p.inputProgramPost)
	programRoute.DELETE("", p.deleteProgram)
	programRoute.PUT("", p.updateProgramPost)
}

func (p *ProgramPostApi) inputProgramPost(c *gin.Context) {
	var newProgramPost dto.ProgramPostRequest
	response := appresponse.NewJsonResponse(c)

	if err := c.ShouldBindJSON(&newProgramPost); err != nil {
		response.SendError(appresponse.NewBadRequestError(err, "Failed to input a new program post"))
		return
	}

	var postedProgram = utils.NewProgramResponse(newProgramPost).MappingProgramPostRequest()

	_, err := p.usecase.InputProgramPost(postedProgram)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed to input a new program post"))
		return
	}

	response.SendData(appresponse.NewResponseMessage("SUCCESS", "Input a new program post", nil))
}

func (p *ProgramPostApi) updateProgramPost(c *gin.Context) {
	var updatedProgramPost dto.ProgramPostRequest
	response := appresponse.NewJsonResponse(c)

	if err := c.ShouldBindJSON(&updatedProgramPost); err != nil {
		response.SendError(appresponse.NewBadRequestError(err, "Failed to update program post"))
		return
	}

	var postedProgram = utils.NewProgramResponse(updatedProgramPost).MappingProgramPostRequest()

	_, err := p.usecase.UpdateProgramPost(postedProgram)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed to update program post"))
		return
	}

	response.SendData(appresponse.NewResponseMessage("SUCCESS", "Updated program post", nil))
}

func (p *ProgramPostApi) getProgram(c *gin.Context) {
	id := c.Query("id")
	name := c.Query("name")
	page := c.Query("page")
	limit := c.Query("limit")
	sortBy := c.Query("sort_by")
	orderBy := c.Query("order_by")

	response := appresponse.NewJsonResponse(c)

	if orderBy != "" {
		if orderBy == "asc" || orderBy == "desc" {
			sortBy = sortBy + " " + orderBy
		} else {
			response.SendError(appresponse.NewBadRequestError(nil, "Only can order by 'asc' or 'desc'"))
		}
	}

	if id != "" {
		program, err := p.usecase.SearchProgramById(id)
		if err != nil {
			if err == apperror.NoRecordFoundErr {
				response.SendData(appresponse.NewNoRecordFoundMessage())
				return
			}
			response.SendError(appresponse.NewInternalServerError(err, "Failed"))
			return
		}
		response.
			SendData(appresponse.NewResponseMessage("SUCCESS", "Program By Id", program))
		return
	}

	if name != "" {
		programList, err := p.usecase.SearchProgramByName(name)
		if err != nil {
			response.SendError(appresponse.NewInternalServerError(err, "Failed"))
			return
		}
		response.
			SendData(appresponse.NewResponseMessage("SUCCESS", "Program By Name", programList))
		return
	}

	if page != "" || limit != "" || sortBy != "" {
		pageInt, err := strconv.Atoi(page)
		if err != nil {
			response.SendError(appresponse.NewBadRequestError(err, "Page must be a number"))
			return
		}

		limitInt, err := strconv.Atoi(limit)
		if err != nil {
			response.SendError(appresponse.NewBadRequestError(err, "Limit must be a number"))
			return
		}

		if sortBy == "" {
			sortBy = "open_date"
		}
		programList, err := p.usecase.GetAllProgramWithPagination(pageInt, limitInt, sortBy)
		if err != nil {
			response.SendError(appresponse.NewInternalServerError(err, "Failed"))
			return
		}
		response.
			SendData(appresponse.NewResponseMessage("SUCCESS", "Program with pagination", programList))
		return
	}

	programList, err := p.usecase.GetAllProgram()
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed"))
		return
	}
	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Program By Name", programList))
}

func (p *ProgramPostApi) deleteProgram(c *gin.Context) {
	id := c.Query("id")
	response := appresponse.NewJsonResponse(c)
	_, err := p.usecase.DeleteProgramPostById(id)

	if err == apperror.NoRecordFoundErr {
		response.SendData(appresponse.NewNoRecordFoundMessage())
		return
	}

	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Program By ID", id))
}
