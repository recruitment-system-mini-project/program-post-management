package delivery

import (
	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/delivery/appresponse"
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/usecase"
	"github.com/gin-gonic/gin"
)

type RecruiterApi struct {
	usecase     usecase.RecruiterUseCase
	publicRoute *gin.RouterGroup
}

func NewRecruiterApi(publicRoute *gin.RouterGroup, useCase usecase.RecruiterUseCase) *RecruiterApi {
	api := RecruiterApi{
		usecase:     useCase,
		publicRoute: publicRoute,
	}

	api.initRouter()
	return &api
}

func (p *RecruiterApi) initRouter() {
	programRoute := p.publicRoute.Group("/recruiter")
	programRoute.GET("", p.getRecruiter)
	programRoute.POST("", p.inputRecruiter)
	programRoute.DELETE("", p.deleteRecruiter)
	programRoute.PUT("", p.updateRecruiter)
}

func (p *RecruiterApi) inputRecruiter(c *gin.Context) {
	var newRecruiter entity.RecruiterProfile
	response := appresponse.NewJsonResponse(c)

	if err := c.ShouldBindJSON(&newRecruiter); err != nil {
		response.SendError(appresponse.NewBadRequestError(err, "Failed to input a new recruiter"))
		return
	}

	_, err := p.usecase.InputRecruiterProfile(newRecruiter)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed to input a new recruiter"))
		return
	}

	response.SendData(appresponse.NewResponseMessage("SUCCESS", "Input a new recruiter", nil))
}

func (p *RecruiterApi) updateRecruiter(c *gin.Context) {
	var updatedRecruiter entity.RecruiterProfile
	response := appresponse.NewJsonResponse(c)

	if err := c.ShouldBindJSON(&updatedRecruiter); err != nil {
		response.SendError(appresponse.NewBadRequestError(err, "Failed to update recruiter profile"))
		return
	}

	_, err := p.usecase.UpdateRecruiterProfile(updatedRecruiter)
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed to update recruiter profile"))
		return
	}

	response.SendData(appresponse.NewResponseMessage("SUCCESS", "Updated recruiter profile", nil))
}

func (p *RecruiterApi) getRecruiter(c *gin.Context) {
	id := c.Query("id")

	response := appresponse.NewJsonResponse(c)

	if id != "" {
		recruiter, err := p.usecase.SearchRecruiterById(id)
		if err != nil {
			if err == apperror.NoRecordFoundErr {
				response.SendData(appresponse.NewNoRecordFoundMessage())
				return
			}
			response.SendError(appresponse.NewInternalServerError(err, "Failed"))
			return
		}
		response.
			SendData(appresponse.NewResponseMessage("SUCCESS", "Recruiter profile by Id", recruiter))
		return
	}

	recruiterList, err := p.usecase.GetAllRecruiter()
	if err != nil {
		response.SendError(appresponse.NewInternalServerError(err, "Failed"))
		return
	}
	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Program By Name", recruiterList))
}

func (p *RecruiterApi) deleteRecruiter(c *gin.Context) {
	id := c.Query("id")
	response := appresponse.NewJsonResponse(c)
	_, err := p.usecase.DeleteRecruiterProfileById(id)

	if err == apperror.NoRecordFoundErr {
		response.SendData(appresponse.NewNoRecordFoundMessage())
		return
	}

	response.
		SendData(appresponse.NewResponseMessage("SUCCESS", "Delete recruiter by ID", id))
}
