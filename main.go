package main

import "enigmacamp.com/recruitment_system/api"

func main() {
	api.NewApiServer().Run()
}
