package apperror

import "errors"

var NoRecordFoundErr = errors.New("No Record Found")
var RecordIsExistErr = errors.New("Record Found Is Exist")
