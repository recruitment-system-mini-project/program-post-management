package dto

type ApplyProgramRequest struct {
	ProgramId   string
	ApplicantId string
}
