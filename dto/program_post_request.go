package dto

type ProgramPostRequest struct {
	ID              string
	ProgramTypeID   string
	ProgramActivity programActivity
	ProgramLocation programLocation
	Headline        string
	Description     string
	Requirement     string
	IsActive        bool
}

type programActivity struct {
	ID            string
	OpenDate      string
	CloseDate     string
	ProcessStatus string
}

type programLocation struct {
	ID       string
	Address  string
	Platform string
}
