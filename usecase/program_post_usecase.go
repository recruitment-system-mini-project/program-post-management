package usecase

import (
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/repository"
)

type ProgramPostUseCase interface {
	GetAllProgram() ([]entity.ProgramPost, error)
	GetAllProgramWithPagination(page int, limit int, sortBy string) ([]entity.ProgramPost, error)
	SearchProgramByName(programName string) ([]entity.ProgramPost, error)
	SearchProgramById(programId string) (*entity.ProgramPost, error)
	InputProgramPost(programPost entity.ProgramPost) (*entity.ProgramPost, error)
	DeleteProgramPostById(id string) (*entity.ProgramPost, error)
	UpdateProgramPost(programPost entity.ProgramPost) (*entity.ProgramPost, error)
}

type programPostUseCase struct {
	programPostRepo repository.ProgramPostRepository
}

func NewProgramPostUseCase(programPostRepo repository.ProgramPostRepository) ProgramPostUseCase {
	return &programPostUseCase{
		programPostRepo: programPostRepo,
	}
}

func (p *programPostUseCase) GetAllProgram() ([]entity.ProgramPost, error) {
	return p.programPostRepo.GetAll()
}

func (p *programPostUseCase) GetAllProgramWithPagination(page int, limit int, sortBy string) ([]entity.ProgramPost, error) {
	return p.programPostRepo.GetAllWithPagination(page, limit, sortBy)
}

func (p *programPostUseCase) SearchProgramByName(programName string) ([]entity.ProgramPost, error) {
	return p.programPostRepo.GetByName(programName)
}

func (p *programPostUseCase) SearchProgramById(programId string) (*entity.ProgramPost, error) {
	return p.programPostRepo.GetById(programId)
}

func (p *programPostUseCase) InputProgramPost(programPost entity.ProgramPost) (*entity.ProgramPost, error) {
	return p.programPostRepo.CreateOne(programPost)
}

func (p *programPostUseCase) DeleteProgramPostById(id string) (*entity.ProgramPost, error) {
	return p.programPostRepo.DeleteOne(id)
}

func (p *programPostUseCase) UpdateProgramPost(programPost entity.ProgramPost) (*entity.ProgramPost, error) {
	return p.programPostRepo.UpdateOne(programPost)
}


