package usecase

import (
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/repository"
)

type ProgramApplicantUseCase interface {
	ApplyProgram(appliedProgram entity.ProgramApplicant) (*entity.ProgramApplicant, error)
	AppliedProgramIsExist(idProgram, idApplicant string) (bool, error)
	GetAllProgramByApplicant(idApplicant string) (*entity.Applicant, error)
	GetAllApplicantByProgram(idProgram string) (*entity.ProgramPost, error)
}

type programApplicantUseCase struct {
	programApplicantRepo repository.ProgramApplicantRepository
}

func NewProgramApplicantUseCase(programApplicantRepo repository.ProgramApplicantRepository) ProgramApplicantUseCase {
	return &programApplicantUseCase{
		programApplicantRepo: programApplicantRepo,
	}
}

func (p *programApplicantUseCase) ApplyProgram(appliedProgram entity.ProgramApplicant) (*entity.ProgramApplicant, error) {
	return p.programApplicantRepo.CreateOne(appliedProgram)
}

func (p *programApplicantUseCase) AppliedProgramIsExist(idProgram, idApplicant string) (bool, error) {
	return p.programApplicantRepo.AppliedIsExist(idProgram, idApplicant)
}

func (p *programApplicantUseCase) GetAllProgramByApplicant(idApplicant string) (*entity.Applicant, error) {
	return p.programApplicantRepo.GetAllProgramByApplicantId(idApplicant)
}

func (p *programApplicantUseCase) GetAllApplicantByProgram(idProgram string) (*entity.ProgramPost, error) {
	return p.programApplicantRepo.GetAllApplicantByProgramId(idProgram)
}
