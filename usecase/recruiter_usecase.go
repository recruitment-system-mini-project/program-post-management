package usecase

import (
	"enigmacamp.com/recruitment_system/entity"
	"enigmacamp.com/recruitment_system/repository"
)

type RecruiterUseCase interface {
	GetAllRecruiter() ([]entity.RecruiterProfile, error)
	SearchRecruiterById(recruiterId string) (*entity.RecruiterProfile, error)
	InputRecruiterProfile(newRecruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error)
	DeleteRecruiterProfileById(id string) (*entity.RecruiterProfile, error)
	UpdateRecruiterProfile(recruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error)
}

type recruiterUseCase struct {
	recruiterRepo repository.RecruiterRepository
}

func NewRecruiterUseCase(recruiterRepo repository.RecruiterRepository) RecruiterUseCase {
	return &recruiterUseCase{
		recruiterRepo: recruiterRepo,
	}
}

func (r *recruiterUseCase) GetAllRecruiter() ([]entity.RecruiterProfile, error) {
	return r.recruiterRepo.GetAll()
}

func (r *recruiterUseCase) SearchRecruiterById(recruiterId string) (*entity.RecruiterProfile, error) {
	return r.recruiterRepo.GetById(recruiterId)
}

func (r *recruiterUseCase) InputRecruiterProfile(recruiterPost entity.RecruiterProfile) (*entity.RecruiterProfile, error) {
	return r.recruiterRepo.CreateOne(recruiterPost)
}

func (r *recruiterUseCase) DeleteRecruiterProfileById(id string) (*entity.RecruiterProfile, error) {
	return r.recruiterRepo.DeleteOne(id)
}

func (r *recruiterUseCase) UpdateRecruiterProfile(recruiterPost entity.RecruiterProfile) (*entity.RecruiterProfile, error) {
	return r.recruiterRepo.UpdateOne(recruiterPost)
}
