package repository

import (
	"fmt"
	"log"

	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/entity"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type ProgramPostRepository interface {
	GetAll() ([]entity.ProgramPost, error)
	GetAllWithPagination(page int, limit int, sortBy string) ([]entity.ProgramPost, error)
	GetByName(name string) ([]entity.ProgramPost, error)
	GetById(id string) (*entity.ProgramPost, error)
	CreateOne(programPost entity.ProgramPost) (*entity.ProgramPost, error)
	DeleteOne(id string) (*entity.ProgramPost, error)
	UpdateOne(program entity.ProgramPost) (*entity.ProgramPost, error)
}

type programRepository struct {
	db *gorm.DB
}

func NewProgramPostRepository(resource *gorm.DB) ProgramPostRepository {
	return &programRepository{
		db: resource,
	}
}

func (p *programRepository) GetAll() ([]entity.ProgramPost, error) {
	var list []entity.ProgramPost

	err := p.db.Preload(clause.Associations).Find(&list).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return list, nil
}

func (p *programRepository) GetAllWithPagination(page int, limit int, sortBy string) ([]entity.ProgramPost, error) {
	offset := (page - 1) * limit
	var list []entity.ProgramPost

	err := p.db.Limit(limit).Offset(offset).Preload(clause.Associations).Find(&list).Order(sortBy).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return list, nil
}

func (p *programRepository) GetById(id string) (*entity.ProgramPost, error) {
	var program entity.ProgramPost

	err := p.db.Where("id = ?", id).Preload(clause.Associations).First(&program).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &program, nil
}

func (p *programRepository) GetByName(name string) ([]entity.ProgramPost, error) {
	var listByName []entity.ProgramPost
	var searchKeyword = fmt.Sprintf("%%%s%%", name)
	err := p.db.Where("headline ilike ?", searchKeyword).Preload(clause.Associations).Find(&listByName).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return listByName, nil
}

func (p *programRepository) CreateOne(programPost entity.ProgramPost) (*entity.ProgramPost, error) {
	err := p.db.Create(&programPost).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &programPost, nil
}

func (p *programRepository) DeleteOne(id string) (*entity.ProgramPost, error) {
	program := entity.ProgramPost{ID: id}
	err := p.db.Delete(&program).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &program, nil
}

func (p *programRepository) UpdateOne(program entity.ProgramPost) (*entity.ProgramPost, error) {

	err := p.db.Model(&program).Updates(&program.ProgramActivity).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &program, nil
}
