package repository

import (
	"log"

	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/entity"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type RecruiterRepository interface {
	CreateOne(newRecruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error)
	DeleteOne(id string) (*entity.RecruiterProfile, error)
	UpdateOne(recruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error)
	GetById(id string) (*entity.RecruiterProfile, error)
	GetAll() ([]entity.RecruiterProfile, error)
}

type recruiterRepository struct {
	db *gorm.DB
}

func NewRecuiterRepository(resource *gorm.DB) RecruiterRepository {
	return &recruiterRepository{
		db: resource,
	}
}

func (r *recruiterRepository) CreateOne(newRecruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error) {
	err := r.db.Create(&newRecruiter).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &newRecruiter, nil
}

func (r *recruiterRepository) DeleteOne(id string) (*entity.RecruiterProfile, error) {
	recruiter := entity.RecruiterProfile{ID: id}
	err := r.db.Delete(&recruiter).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &recruiter, nil
}

func (p *recruiterRepository) UpdateOne(recruiter entity.RecruiterProfile) (*entity.RecruiterProfile, error) {

	err := p.db.Model(&recruiter).Where("id = ?", recruiter.ID).Updates(&recruiter).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &recruiter, nil
}

func (r *recruiterRepository) GetById(id string) (*entity.RecruiterProfile, error) {
	var recruiter entity.RecruiterProfile

	err := r.db.Where("id = ?", id).Preload(clause.Associations).First(&recruiter).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &recruiter, nil
}

func (p *recruiterRepository) GetAll() ([]entity.RecruiterProfile, error) {
	var list []entity.RecruiterProfile

	err := p.db.Preload(clause.Associations).Find(&list).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return list, nil
}
