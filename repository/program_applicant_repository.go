package repository

import (
	"log"

	"enigmacamp.com/recruitment_system/apperror"
	"enigmacamp.com/recruitment_system/entity"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type ProgramApplicantRepository interface {
	// GetAll() ([]entity.ProgramPost, error)
	// GetAllWithPagination(page int, limit int, sortBy string) ([]entity.ProgramPost, error)
	// GetByName(name string) ([]entity.ProgramPost, error)
	// GetById(id string) (*entity.ProgramPost, error)
	CreateOne(appliedProgram entity.ProgramApplicant) (*entity.ProgramApplicant, error)
	AppliedIsExist(idProgram, idApplicant string) (bool, error)
	GetAllProgramByApplicantId(id string) (*entity.Applicant, error)
	GetAllApplicantByProgramId(id string) (*entity.ProgramPost, error)
	// DeleteOne(id string) (*entity.ProgramPost, error)
	// UpdateOne(program entity.ProgramPost) (*entity.ProgramPost, error)
}

type programApplicantRepository struct {
	db *gorm.DB
}

func NewProgramApplicantRepository(resource *gorm.DB) ProgramApplicantRepository {
	return &programApplicantRepository{
		db: resource,
	}
}

func (p *programApplicantRepository) AppliedIsExist(idProgram, idApplicant string) (bool, error) {

	result := p.db.Where("program_post_id=? AND applicant_id=? AND deleted_at IS NULL", idProgram, idApplicant).Find(&entity.ProgramApplicant{})
	err := result.Error

	if err != nil {
		log.Println(err)
		return true, err
	}

	if result.RowsAffected != 0 {
		return true, apperror.RecordIsExistErr
	}

	log.Println("CEK EXISTING", idProgram, idApplicant, result.RowsAffected)

	return false, nil
}

func (p *programApplicantRepository) CreateOne(appliedProgram entity.ProgramApplicant) (*entity.ProgramApplicant, error) {
	applicant := entity.Applicant{
		ID: appliedProgram.ApplicantId,
		ProgramPosts: []entity.ProgramPost{
			{
				ID: appliedProgram.ProgramPostId,
			},
		},
	}

	tx := p.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	err := tx.Create(&appliedProgram).Error
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return nil, err
	}

	err = tx.Model(&applicant).Updates(&applicant).Error
	if err != nil {
		log.Println(err)
		tx.Rollback()
		return nil, err
	}

	tx.Commit()

	return &appliedProgram, nil
}

func (p *programApplicantRepository) GetAllProgramByApplicantId(id string) (*entity.Applicant, error) {
	var applicant entity.Applicant
	err := p.db.Where("id = ?", id).Preload("ProgramPosts." + clause.Associations).First(&applicant).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &applicant, nil
}

func (p *programApplicantRepository) GetAllApplicantByProgramId(id string) (*entity.ProgramPost, error) {
	var program entity.ProgramPost
	err := p.db.Where("id = ?", id).Preload(clause.Associations).First(&program).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, apperror.NoRecordFoundErr
		}
		log.Println(err)
		return nil, err
	}
	return &program, nil
}
