package config

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

type Config struct {
	RouterEngine   *gin.Engine
	ApiBaseUrl     string
	RunMigration   string
	DataSourceName string
}

func NewConfig() *Config {
	config := new(Config)
	runMigration := getEnvVariable("DB_MIGRATION")
	apiHost := getEnvVariable("API_HOST")
	apiPort := getEnvVariable("API_PORT")

	dbHost := getEnvVariable("DB_HOST")
	dbPort := getEnvVariable("DB_PORT")
	dbName := getEnvVariable("DB_NAME")
	dbUser := getEnvVariable("DB_USER")
	dbPassword := getEnvVariable("DB_PASSWORD")

	// dataSorceName := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", dbUser, dbPassword, dbHost, dbPort, dbName)
	dataSorceName := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", dbHost, dbUser, dbPassword, dbName, dbPort)
	config.DataSourceName = dataSorceName

	r := gin.Default()
	config.RouterEngine = r

	config.ApiBaseUrl = fmt.Sprintf("%s:%s", apiHost, apiPort)
	config.RunMigration = runMigration
	return config
}

func getEnvVariable(key string) string {
	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}
